﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.table = new System.Windows.Forms.DataGridView();
            this.Mjerenja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxMjerenja = new System.Windows.Forms.TextBox();
            this.textBoxAlternativa = new System.Windows.Forms.TextBox();
            this.labelMjerenje = new System.Windows.Forms.Label();
            this.labelAlternativa = new System.Windows.Forms.Label();
            this.buttonTabla = new System.Windows.Forms.Button();
            this.tabla2 = new System.Windows.Forms.DataGridView();
            this.start = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ukupno = new System.Windows.Forms.Label();
            this.ssa = new System.Windows.Forms.Label();
            this.sse = new System.Windows.Forms.Label();
            this.sst = new System.Windows.Forms.Label();
            this.sa = new System.Windows.Forms.Label();
            this.se = new System.Windows.Forms.Label();
            this.final = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.al1 = new System.Windows.Forms.Label();
            this.al2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabla2)).BeginInit();
            this.SuspendLayout();
            // 
            // table
            // 
            this.table.AllowUserToAddRows = false;
            this.table.AllowUserToDeleteRows = false;
            this.table.AllowUserToResizeColumns = false;
            this.table.AllowUserToResizeRows = false;
            this.table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Mjerenja});
            this.table.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.table.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.table.Location = new System.Drawing.Point(238, 86);
            this.table.Name = "table";
            this.table.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.table.ShowEditingIcon = false;
            this.table.Size = new System.Drawing.Size(541, 301);
            this.table.TabIndex = 0;
            // 
            // Mjerenja
            // 
            this.Mjerenja.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Mjerenja.HeaderText = "Mjerenja";
            this.Mjerenja.MaxInputLength = 20;
            this.Mjerenja.Name = "Mjerenja";
            this.Mjerenja.ReadOnly = true;
            this.Mjerenja.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Mjerenja.Width = 53;
            // 
            // textBoxMjerenja
            // 
            this.textBoxMjerenja.Location = new System.Drawing.Point(84, 15);
            this.textBoxMjerenja.Name = "textBoxMjerenja";
            this.textBoxMjerenja.Size = new System.Drawing.Size(100, 20);
            this.textBoxMjerenja.TabIndex = 1;
            // 
            // textBoxAlternativa
            // 
            this.textBoxAlternativa.Location = new System.Drawing.Point(84, 49);
            this.textBoxAlternativa.Name = "textBoxAlternativa";
            this.textBoxAlternativa.Size = new System.Drawing.Size(100, 20);
            this.textBoxAlternativa.TabIndex = 2;
            // 
            // labelMjerenje
            // 
            this.labelMjerenje.AutoSize = true;
            this.labelMjerenje.Location = new System.Drawing.Point(1, 15);
            this.labelMjerenje.Name = "labelMjerenje";
            this.labelMjerenje.Size = new System.Drawing.Size(70, 13);
            this.labelMjerenje.TabIndex = 3;
            this.labelMjerenje.Text = "Broj mjerenja:";
            // 
            // labelAlternativa
            // 
            this.labelAlternativa.AutoSize = true;
            this.labelAlternativa.Location = new System.Drawing.Point(1, 52);
            this.labelAlternativa.Name = "labelAlternativa";
            this.labelAlternativa.Size = new System.Drawing.Size(83, 13);
            this.labelAlternativa.TabIndex = 4;
            this.labelAlternativa.Text = "Broj alternativa: \r\n";
            // 
            // buttonTabla
            // 
            this.buttonTabla.Location = new System.Drawing.Point(38, 86);
            this.buttonTabla.Name = "buttonTabla";
            this.buttonTabla.Size = new System.Drawing.Size(102, 23);
            this.buttonTabla.TabIndex = 5;
            this.buttonTabla.Text = "Kreiraj tabelu";
            this.buttonTabla.UseVisualStyleBackColor = true;
            this.buttonTabla.Click += new System.EventHandler(this.buttonTabla_Click);
            // 
            // tabla2
            // 
            this.tabla2.AllowUserToAddRows = false;
            this.tabla2.AllowUserToDeleteRows = false;
            this.tabla2.AllowUserToResizeColumns = false;
            this.tabla2.AllowUserToResizeRows = false;
            this.tabla2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tabla2.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.tabla2.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tabla2.Location = new System.Drawing.Point(238, 405);
            this.tabla2.Name = "tabla2";
            this.tabla2.ReadOnly = true;
            this.tabla2.RowHeadersVisible = false;
            this.tabla2.ShowEditingIcon = false;
            this.tabla2.Size = new System.Drawing.Size(541, 91);
            this.tabla2.TabIndex = 6;
            this.tabla2.Visible = false;
            this.tabla2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(38, 136);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(102, 23);
            this.start.TabIndex = 7;
            this.start.Text = "Start ";
            this.start.UseVisualStyleBackColor = true;
            this.start.Visible = false;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(238, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "Popunite tabelu:";
            this.label1.Visible = false;
            // 
            // ukupno
            // 
            this.ukupno.AutoSize = true;
            this.ukupno.Location = new System.Drawing.Point(13, 172);
            this.ukupno.Name = "ukupno";
            this.ukupno.Size = new System.Drawing.Size(86, 13);
            this.ukupno.TabIndex = 9;
            this.ukupno.Text = "Ukupna sr. vr. = ";
            this.ukupno.Visible = false;
            // 
            // ssa
            // 
            this.ssa.AutoSize = true;
            this.ssa.Location = new System.Drawing.Point(13, 199);
            this.ssa.Name = "ssa";
            this.ssa.Size = new System.Drawing.Size(40, 13);
            this.ssa.TabIndex = 10;
            this.ssa.Text = "SSA = ";
            this.ssa.Visible = false;
            // 
            // sse
            // 
            this.sse.AutoSize = true;
            this.sse.Location = new System.Drawing.Point(13, 224);
            this.sse.Name = "sse";
            this.sse.Size = new System.Drawing.Size(40, 13);
            this.sse.TabIndex = 11;
            this.sse.Text = "SSE = ";
            this.sse.Visible = false;
            // 
            // sst
            // 
            this.sst.AutoSize = true;
            this.sst.Location = new System.Drawing.Point(13, 246);
            this.sst.Name = "sst";
            this.sst.Size = new System.Drawing.Size(40, 13);
            this.sst.TabIndex = 12;
            this.sst.Text = "SST = ";
            this.sst.Visible = false;
            // 
            // sa
            // 
            this.sa.AutoSize = true;
            this.sa.Location = new System.Drawing.Point(13, 269);
            this.sa.Name = "sa";
            this.sa.Size = new System.Drawing.Size(44, 13);
            this.sa.TabIndex = 13;
            this.sa.Text = "Sa^2 = ";
            this.sa.Visible = false;
            // 
            // se
            // 
            this.se.AutoSize = true;
            this.se.Location = new System.Drawing.Point(13, 294);
            this.se.Name = "se";
            this.se.Size = new System.Drawing.Size(44, 13);
            this.se.TabIndex = 14;
            this.se.Text = "Se^2 = ";
            this.se.Visible = false;
            // 
            // final
            // 
            this.final.AutoSize = true;
            this.final.Location = new System.Drawing.Point(16, 324);
            this.final.Name = "final";
            this.final.Size = new System.Drawing.Size(0, 13);
            this.final.TabIndex = 15;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(38, 405);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 16;
            this.comboBox1.Visible = false;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(38, 432);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 17;
            this.comboBox2.Visible = false;
            // 
            // al1
            // 
            this.al1.AutoSize = true;
            this.al1.Location = new System.Drawing.Point(11, 408);
            this.al1.Name = "al1";
            this.al1.Size = new System.Drawing.Size(21, 13);
            this.al1.TabIndex = 18;
            this.al1.Text = "al1";
            this.al1.Visible = false;
            // 
            // al2
            // 
            this.al2.AutoSize = true;
            this.al2.Location = new System.Drawing.Point(11, 435);
            this.al2.Name = "al2";
            this.al2.Size = new System.Drawing.Size(21, 13);
            this.al2.TabIndex = 19;
            this.al2.Text = "al2";
            this.al2.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(38, 459);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(64, 486);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "label4";
            this.label4.Visible = false;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(71, 386);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 15);
            this.label2.TabIndex = 22;
            this.label2.Text = "Kontrasti:";
            this.label2.Visible = false;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Location = new System.Drawing.Point(68, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 15);
            this.label3.TabIndex = 23;
            this.label3.Text = "FTest:";
            this.label3.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 508);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.al2);
            this.Controls.Add(this.al1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.final);
            this.Controls.Add(this.se);
            this.Controls.Add(this.sa);
            this.Controls.Add(this.sst);
            this.Controls.Add(this.sse);
            this.Controls.Add(this.ssa);
            this.Controls.Add(this.ukupno);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.start);
            this.Controls.Add(this.tabla2);
            this.Controls.Add(this.buttonTabla);
            this.Controls.Add(this.labelAlternativa);
            this.Controls.Add(this.labelMjerenje);
            this.Controls.Add(this.textBoxAlternativa);
            this.Controls.Add(this.textBoxMjerenja);
            this.Controls.Add(this.table);
            this.Name = "Form1";
            this.Text = "ANOVA";
            ((System.ComponentModel.ISupportInitialize)(this.table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabla2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView table;
        private System.Windows.Forms.TextBox textBoxMjerenja;
        private System.Windows.Forms.TextBox textBoxAlternativa;
        private System.Windows.Forms.Label labelMjerenje;
        private System.Windows.Forms.Label labelAlternativa;
        private System.Windows.Forms.Button buttonTabla;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mjerenja;
        private System.Windows.Forms.DataGridView tabla2;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ukupno;
        private System.Windows.Forms.Label ssa;
        private System.Windows.Forms.Label sse;
        private System.Windows.Forms.Label sst;
        private System.Windows.Forms.Label sa;
        private System.Windows.Forms.Label se;
        private System.Windows.Forms.Label final;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label al1;
        private System.Windows.Forms.Label al2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

